const express = require("express");

const app = express();
const port = 8002;

app.use(express.json());
// app.use

app.get("/", async (req, res) => {
    console.log('got request')
  res.json({ from: "server2" });
});

app.post("/set", async (req, res) => {
  const { key, value } = req.body;

  // redisClient.set(key,value)
  // .then(response=>{
  //     console.log("response of redis:",response)
  //     res.send(response)
  // })
  // .catch(err=>{
  //     res.send("some problem occurred.." + err)
  // })

  res.send("ok");
});

app.get("/get/:key", async (req, res) => {
  const { key } = req.params;

//   redisClient
//     .get(key)
//     .then((response) => {
//       console.log("response of redis:", response);
//       if (response === null) {
//         res.send("key doesn't exist");
//         return;
//       }
//       res.send(response);
//     })
//     .catch((err) => {
//       res.send("some problem occurred.." + err);
//     });
res.send("OK")
});

app.delete("/del/:key", async (req, res) => {
  const { key } = req.params;

  redisClient
    .del(key)
    .then((response) => {
      console.log("response of redis:", response);
      res.send("ok");
    })
    .catch((err) => {
      res.send("some problem occurred.." + err);
    });
});

app.listen(port, () => console.log("Server running on port ", port));
