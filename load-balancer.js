const { default: axios } = require("axios");
const express = require("express");

const app = express();
const PORT = 8000;

app.use(express.json());
// app.use

app.get("/:server", async (req, res) => {
  const { server } = req.params;

  const isValidServer = [1, 2, 3].includes(Number(server));

  if (!isValidServer) {
    res.send("not a valid request");
    return;
  }

  axios
    .get(`http://localhost:800${server}`)
    .then((response) => {
      console.log("status",response.status)
      console.log("data", response.data);
      res.json(response.data);
    })
    .catch((err) => {
      console.log(err)
      res.send("Error in server:", server);
    });
});

app.post("/set", async (req, res) => {
  const { key, value } = req.body;

  // redisClient.set(key,value)
  // .then(response=>{
  //     console.log("response of redis:",response)
  //     res.send(response)
  // })
  // .catch(err=>{
  //     res.send("some problem occurred.." + err)
  // })

  res.send("ok");
});

app.get("/get/:key", async (req, res) => {
  const { key } = req.params;

  //   redisClient
  //     .get(key)
  //     .then((response) => {
  //       console.log("response of redis:", response);
  //       if (response === null) {
  //         res.send("key doesn't exist");
  //         return;
  //       }
  //       res.send(response);
  //     })
  //     .catch((err) => {
  //       res.send("some problem occurred.." + err);
  //     });
  res.send("OK");
});

app.delete("/del/:key", async (req, res) => {
  const { key } = req.params;

  redisClient
    .del(key)
    .then((response) => {
      console.log("response of redis:", response);
      res.send("ok");
    })
    .catch((err) => {
      res.send("some problem occurred.." + err);
    });
});

app.listen(PORT, () => console.log("Server running on port ", PORT));
